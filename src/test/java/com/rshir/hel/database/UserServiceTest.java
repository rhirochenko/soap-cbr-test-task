package com.rshir.hel.database;

import java.util.List;
import com.rshir.hel.database.User;
import com.rshir.hel.database.UserService;
import junit.framework.Assert;
import junit.framework.TestCase;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

public class UserServiceTest extends TestCase
{
    private static UserService userService;

    @BeforeClass
    public static void setup()
    {
        userService = new UserService();
    }

    @AfterClass
    public static void teardown()
    {
        userService = null;
    }

    @Test
    public void testGetUserById()
    {
        userService = new UserService();
        User user = userService.getUserById(5);
        Assert.assertNotNull(user);
        System.out.println(user);
    }

    @Test
    public void testGetAllUsers()
    {
        userService = new UserService();
        List<User> users = userService.getAllUsers();
        Assert.assertNotNull(users);
        for (User user : users)
        {
            System.out.println(user);
        }

    }

    @Test
    public void testInsertUser()
    {

        User user = new User();
        user.setEmailId("test1_email_"+System.currentTimeMillis()+"@gmail.com");
        user.setPassword("secret");
        user.setFirstName("TestFirstName");
        user.setLastName("TestLastName");

        userService = new UserService();
        userService.insertUser(user);
        Assert.assertTrue(user.getUserId() != 0);
        User createdUser = userService.getUserById(user.getUserId());
        Assert.assertNotNull(createdUser);
        Assert.assertEquals(user.getEmailId(), createdUser.getEmailId());
        Assert.assertEquals(user.getPassword(), createdUser.getPassword());
        Assert.assertEquals(user.getFirstName(), createdUser.getFirstName());
        Assert.assertEquals(user.getLastName(), createdUser.getLastName());

    }

    @Test
    public void testUpdateUser()
    {
        long timestamp = System.currentTimeMillis();
        User user = userService.getUserById(5);
        user.setFirstName("TestFirstName"+timestamp);
        user.setLastName("TestLastName"+timestamp);
        userService = new UserService();
        userService.updateUser(user);
        User updatedUser = userService.getUserById(5);
        Assert.assertEquals(user.getFirstName(), updatedUser.getFirstName());
        Assert.assertEquals(user.getLastName(), updatedUser.getLastName());
    }

    @Test
    public void testDeleteUser()
    {
        userService = new UserService();
        User user = userService.getUserById(5);
        userService.deleteUser(user.getUserId());
        User deletedUser = userService.getUserById(5);
        Assert.assertNull(deletedUser);

    }
}