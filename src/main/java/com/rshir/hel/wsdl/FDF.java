package com.rshir.hel.wsdl;

import javax.xml.bind.annotation.*;

/**
 * FDF class describe one data entry <FDF></FDF>
 * from Data101FullXML response
 */
@XmlRootElement(name = "FDF")
@XmlAccessorType (XmlAccessType.FIELD)
public class FDF
{
    private String dt;
    private String pln;
    private String ap;
    private String vr;
    private String vv;
    private String vitg;
    private String ora;
    private String ova;
    private String oitga;
    private String orp;
    private String ovp;
    private String oitgp;
    private String ir;
    private String iv;
    private String iitg;

    public String getDt() {
        return dt;
    }

    public void setDt(String dt) {
        this.dt = dt;
    }

    public String getPln() {
        return pln;
    }

    public String getAp() {
        return ap;
    }

    public String getVr() {
        return vr;
    }

    public String getVv() {
        return vv;
    }

    public String getVitg() {
        return vitg;
    }

    public String getOra() {
        return ora;
    }

    public String getOva() {
        return ova;
    }

    public String getOitga() {
        return oitga;
    }

    public String getOrp() {
        return orp;
    }

    public String getOvp() {
        return ovp;
    }

    public String getOitgp() {
        return oitgp;
    }

    public String getIr() {
        return ir;
    }

    public String getIv() {
        return iv;
    }

    public String getIitg() {
        return iitg;
    }

    public void setPln(String pln) {
        this.pln = pln;
    }

    public void setAp(String ap) {
        this.ap = ap;
    }

    public void setVr(String vr) {
        this.vr = vr;
    }

    public void setVv(String vv) {
        this.vv = vv;
    }

    public void setVitg(String vitg) {
        this.vitg = vitg;
    }

    public void setOra(String ora) {
        this.ora = ora;
    }

    public void setOva(String ova) {
        this.ova = ova;
    }

    public void setOitga(String oitga) {
        this.oitga = oitga;
    }

    public void setOrp(String orp) {
        this.orp = orp;
    }

    public void setOvp(String ovp) {
        this.ovp = ovp;
    }

    public void setOitgp(String oitgp) {
        this.oitgp = oitgp;
    }

    public void setIr(String ir) {
        this.ir = ir;
    }

    public void setIv(String iv) {
        this.iv = iv;
    }

    public void setIitg(String iitg) {
        this.iitg = iitg;
    }
}
