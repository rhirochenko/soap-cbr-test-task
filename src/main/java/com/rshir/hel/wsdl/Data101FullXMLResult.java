package com.rshir.hel.wsdl;

        import javax.xml.bind.annotation.*;
        import java.util.ArrayList;
        import java.util.List;

/**
 * This class defines XML SOAP response
 * scheme for <Data101FullXMLResult></Data101FullXMLResult>
 */
@XmlRootElement(name = "Data101FullXMLResult")
@XmlAccessorType(XmlAccessType.FIELD)
public class Data101FullXMLResult {
    @XmlElementWrapper(name = "F101DATA")
    @XmlElement(name = "FDF")
    private List<FDF> FDFs = null;

    public List<FDF> getFDFs() {
        return FDFs;
    }

    public void setFDFs(List<FDF> FDFs) {
        this.FDFs = FDFs;
    }

}
