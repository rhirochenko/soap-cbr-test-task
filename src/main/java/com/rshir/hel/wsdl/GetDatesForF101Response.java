package com.rshir.hel.wsdl;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;

/**
 * GetDatesForF101Response class defines scheme for
 * GetDatesForF101Response command response
 */
@XmlRootElement(name = "GetDatesForF101Response", namespace = "http://web.cbr.ru/")
@XmlAccessorType(XmlAccessType.FIELD)
public class GetDatesForF101Response {
    @XmlElementWrapper(name = "GetDatesForF101Result", namespace = "http://web.cbr.ru/")
    @XmlElement(name = "dateTime", namespace = "http://web.cbr.ru/")
    private List<String> GetDatesForF101Result;

    public GetDatesForF101Response() {
        GetDatesForF101Result = new ArrayList<>();
    }

    public void setGetDatesForF101Result(String datetime) {
        GetDatesForF101Result.add(datetime);
    }

    public List<String> getGetDatesForF101Result() {
        return GetDatesForF101Result;
    }
}
