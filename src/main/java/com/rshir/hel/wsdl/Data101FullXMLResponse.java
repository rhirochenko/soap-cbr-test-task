package com.rshir.hel.wsdl;

import com.rshir.hel.requests.Data101FullXMLRequest;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;

/**
 * This class defines XML SOAP response
 * scheme for Data101FullXMLResponse command
 */
@XmlRootElement(name = "Data101FullXMLResponse")
@XmlAccessorType(XmlAccessType.FIELD)
public class Data101FullXMLResponse {
    @XmlElement(name = "Data101FullXMLResult")
    private Data101FullXMLResult data101FullXMLResult;

    public Data101FullXMLResult getData101FullXMLResult() {
        return data101FullXMLResult;
    }

    public void setData101FullXMLResult(Data101FullXMLResult data101FullXMLResult) {
        this.data101FullXMLResult = data101FullXMLResult;
    }

}

