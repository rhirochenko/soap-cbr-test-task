package com.rshir.hel;

import com.rshir.hel.requests.*;
import com.rshir.hel.wsdl.*;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import java.util.List;

@SpringBootApplication
public class Application {

    public static void main(String args[]) throws Exception {
        /* Get DateFrom and DateTo for credprgNumber "1481" */
        GetDatesForF101Request dates = new GetDatesForF101Request("1481");
        String getDateswsdlURL = "http://www.cbr.ru/CreditInfoWebServ/CreditOrgInfo.asmx?op=GetDatesForF101";
        SOAPRequest soapRequest2 = new SOAPRequest(dates,"GetDatesForF101", getDateswsdlURL);
        Object soapDataObject2 = soapRequest2.getResponseObject();
        GetDatesForF101Response datesResponse = (GetDatesForF101Response) soapDataObject2;
        List<String> datesList = datesResponse.getGetDatesForF101Result();
        String dateFrom = datesList.get(0);
        String dateTo = datesList.get(datesList.size()-1);
        System.out.println("DateFrom: "+ dateFrom);
        System.out.println("DateTo: "+ dateTo);

        /* Get Data101Full results */
        Data101FullXMLRequest data101 = new Data101FullXMLRequest("1481","10605",
                dateFrom, dateTo);
        String data101wsdlURL = "http://www.cbr.ru/CreditInfoWebServ/CreditOrgInfo.asmx?op=Data101FullV2XML";
        SOAPRequest soapReqest = new SOAPRequest(data101,"Data101FullXMLResponse", data101wsdlURL);
        Object soapDataObject = soapReqest.getResponseObject();
        Data101FullXMLResponse mobj = (Data101FullXMLResponse) soapDataObject;
        FDF testFDF = mobj.getData101FullXMLResult().getFDFs().get(1);
        System.out.println("\n AP: "+testFDF.getAp());
        System.out.println("Oitgp: "+testFDF.getOitgp());
    }
}
