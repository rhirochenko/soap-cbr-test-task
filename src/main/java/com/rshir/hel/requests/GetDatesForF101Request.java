package com.rshir.hel.requests;

import com.rshir.hel.wsdl.GetDatesForF101Response;

import javax.xml.soap.*;

public class GetDatesForF101Request extends Request{
    /**
     * Credit program number variable
     */
    String CredprgNumber;

    public GetDatesForF101Request(String credprgNumber) {
        CredprgNumber = credprgNumber;
    }

    /**
     * Make GetDatesForF101 SOAP request
     * @return soapMessage
     * @throws Exception
     */
    public SOAPMessage createSOAPRequest() throws Exception {
        MessageFactory messageFactory = MessageFactory.newInstance();
        SOAPMessage soapMessage = messageFactory.createMessage();
        SOAPPart soapPart = soapMessage.getSOAPPart();

        String serverURI = "http://web.cbr.ru/";

        // SOAP Envelope
        SOAPEnvelope envelope = soapPart.getEnvelope();
        //envelope.addNamespaceDeclaration("web", serverURI);

        // SOAP Body
        SOAPBody soapBody = envelope.getBody();
        SOAPElement soapBodyElem = soapBody.addChildElement("GetDatesForF101","","http://web.cbr.ru/");
        SOAPElement soapBodyElem1 = soapBodyElem.addChildElement("CredprgNumber");
        soapBodyElem1.addTextNode(CredprgNumber);

        MimeHeaders headers = soapMessage.getMimeHeaders();
        headers.addHeader("SOAPAction", serverURI  + "GetDatesForF101");

        soapMessage.saveChanges();

        /* Print the request message */
        System.out.print("Request SOAP Message:");
        soapMessage.writeTo(System.out);
        System.out.println();

        return soapMessage;
    }
}