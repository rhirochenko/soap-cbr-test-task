package com.rshir.hel.requests;

import com.rshir.hel.wsdl.Data101FullXMLResponse;
import com.rshir.hel.wsdl.GetDatesForF101Response;
import com.rshir.hel.wsdl.NamespaceFilter;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLReaderFactory;

import javax.xml.bind.*;
import javax.xml.soap.*;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.sax.SAXSource;
import javax.xml.transform.stream.StreamResult;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.StringWriter;
import java.nio.charset.StandardCharsets;

/**
This class is create SOAP request to WSDL service
 */
public class SOAPRequest {
    Request request;
    String requestName;
    String requestURL;

    public SOAPRequest(Request request, String requestName, String requestURL) {
        this.request = request;
        this.requestName = requestName;
        this.requestURL = requestURL;
    }

    /**
     * The method return unmarshalled response object
     * (needs to be casted to use further)
     */
    public Object getResponseObject() throws Exception {
        Object responseObject = new Object();
        if (requestName == "Data101FullXMLResponse") {
            SOAPMessage soapMessage = getSoapResponse();
            SOAPPart sp = soapMessage.getSOAPPart();
            InputStream is = convertSoapBodyToStream(sp);
            responseObject = unmarshall(is, Data101FullXMLResponse.class);
        }
        if (requestName == "GetDatesForF101"){
            SOAPMessage soapMessage = getSoapResponse();
            responseObject = unmarshallGetDates(soapMessage);
        }
        return responseObject;
    }

    public SOAPMessage getSoapResponse() throws Exception {
        // Create SOAP Connection
        SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory.newInstance();
        SOAPConnection soapConnection = soapConnectionFactory.createConnection();

        // Send SOAP Message to SOAP Server
        SOAPMessage soapResponse = soapConnection.call(request.createSOAPRequest(), requestURL);

        SOAPPart sp = soapResponse.getSOAPPart();
        SOAPEnvelope se = sp.getEnvelope();
        SOAPBody sb = se.getBody();
        SOAPHeader sh = se.getHeader();

        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        soapResponse.writeTo(stream);

        soapConnection.close();
        return soapResponse;
    }

    /**
     * Method for ummarshaling Data101FulXMLRequest
     */
    private Object unmarshall(InputStream isStream, Class responseClass) {
        Object myJaxbObject = new Object();
        try {
            //Prepare JAXB objects
            JAXBContext jc = JAXBContext.newInstance(Data101FullXMLResponse.class);

            Unmarshaller u = jc.createUnmarshaller();

            //Create an XMLReader to use with our filter
            XMLReader reader = XMLReaderFactory.createXMLReader();

            //Create the filter (to add namespace) and set the xmlReader as its parent.
            NamespaceFilter inFilter = new NamespaceFilter(null, false);
            inFilter.setParent(reader);
            InputSource inSource = new InputSource(isStream);

            //Create a SAXSource specifying the filter
            SAXSource source = new SAXSource(inFilter, inSource);

            //Do unmarshalling
            //Data101FullXMLResponse st = u.unmarshal(source);
            myJaxbObject = u.unmarshal(source);
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (JAXBException e) {
            e.printStackTrace();
        }
        return myJaxbObject;
    }

    /**
     * Method for ummarshaling GetDatesFor101Request
     */
    private Object unmarshallGetDates(SOAPMessage soapResponse) throws Exception {
        JAXBContext jaxbContext = JAXBContext.newInstance(GetDatesForF101Response.class);
        Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
        GetDatesForF101Response updateMyDetails = (GetDatesForF101Response) jaxbUnmarshaller.unmarshal(soapResponse.getSOAPBody().extractContentAsDocument());
        return updateMyDetails;
    }

    /**
     * Converting SOAPBody object to Input stream
     * (in order to use attribute filter)
     */
    private InputStream convertSoapBodyToStream(SOAPPart soapMessage) {
        DOMSource source2 = new DOMSource(soapMessage);
        StringWriter stringResult = new StringWriter();
        try {
            TransformerFactory.newInstance().newTransformer().transform(source2, new StreamResult(stringResult));
        } catch (TransformerConfigurationException e) {
            e.printStackTrace();
        } catch (TransformerException e) {
            e.printStackTrace();
        }
        String message = stringResult.toString();
        message = getElementAsString(message, requestName);
        InputStream inputStream = new ByteArrayInputStream(message.getBytes(StandardCharsets.UTF_8));
        return inputStream;
    }

    /**
     * Get only XML elements that we can parse
     * (avoid additional wrappers xml elements)
     */
    private String getElementAsString(String xml, String tagName){
        int beginIndex = xml.indexOf("<" + tagName);
        int endIndex = xml.indexOf("</" + tagName, beginIndex) + tagName.length() + 3;
        return xml.substring(beginIndex, endIndex);
    }
}