package com.rshir.hel.requests;

import javax.xml.soap.*;

public class Data101FullXMLRequest extends Request {
    String CredorgNumber;
    String IndCode;
    String DateFrom;
    String DateTo;

    public Data101FullXMLRequest(String credorgNumber, String indCode,
                                 String dateFrom, String dateTo) {
        CredorgNumber = credorgNumber;
        IndCode = indCode;
        DateFrom = dateFrom;
        DateTo = dateTo;
    }

    /**
     * Make Data101FullXML SOAP request
     * @return soapMessage
     * @throws Exception
     */
    public SOAPMessage createSOAPRequest() throws Exception {
        MessageFactory messageFactory = MessageFactory.newInstance();
        SOAPMessage soapMessage = messageFactory.createMessage();
        SOAPPart soapPart = soapMessage.getSOAPPart();

        String serverURI = "http://web.cbr.ru/";

        // SOAP Envelope
        SOAPEnvelope envelope = soapPart.getEnvelope();
        //envelope.addNamespaceDeclaration("web", serverURI);

        // SOAP Body
        SOAPBody soapBody = envelope.getBody();
        SOAPElement soapBodyElem = soapBody.addChildElement("Data101FullXML","","http://web.cbr.ru/");
        SOAPElement soapBodyElem1 = soapBodyElem.addChildElement("CredorgNumber");
        soapBodyElem1.addTextNode(this.CredorgNumber);
        SOAPElement soapBodyElem2 = soapBodyElem.addChildElement("IndCode");
        soapBodyElem2.addTextNode(this.IndCode);
        SOAPElement soapBodyElem3 = soapBodyElem.addChildElement("DateFrom");
        soapBodyElem3.addTextNode(this.DateFrom);
        SOAPElement soapBodyElem4 = soapBodyElem.addChildElement("DateTo");
        soapBodyElem4.addTextNode(this.DateTo);

        MimeHeaders headers = soapMessage.getMimeHeaders();
        headers.addHeader("SOAPAction", serverURI  + "Data101FullXML");

        soapMessage.saveChanges();

        /* Print the request message */
        System.out.print("Request SOAP Message:");
        soapMessage.writeTo(System.out);
        System.out.println();

        return soapMessage;
    }
}
