package com.rshir.hel.database.mappers;


import java.util.List;

import com.rshir.hel.database.Form101;
import com.rshir.hel.wsdl.FDF;

public interface Form101Mapper
{

    public void insertForm101(Form101 form101);
    public void insertFDF(FDF fdf);
}