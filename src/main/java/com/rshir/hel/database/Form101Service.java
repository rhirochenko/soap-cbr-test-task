package com.rshir.hel.database;

import com.rshir.hel.database.mappers.Form101Mapper;
import com.rshir.hel.database.mappers.UserMapper;
import com.rshir.hel.wsdl.FDF;
import org.apache.ibatis.session.SqlSession;

import java.util.List;

import com.rshir.hel.database.mappers.UserMapper;
import org.apache.ibatis.session.SqlSession;

import java.util.List;

public class Form101Service
{
    public void insertForm101(Form101 form101) {
        SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
        try{
            Form101Mapper form101Mapper = sqlSession.getMapper(Form101Mapper.class);
            form101Mapper.insertForm101(form101);
            sqlSession.commit();
        }finally{
            sqlSession.close();
        }
    }

    public void insertFDF(FDF fdf) {
        SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
        try{
            Form101Mapper form101Mapper = sqlSession.getMapper(Form101Mapper.class);
            form101Mapper.insertFDF(fdf);
            sqlSession.commit();
        }finally{
            sqlSession.close();
        }
    }
}