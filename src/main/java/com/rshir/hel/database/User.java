package com.rshir.hel.database;

public class User
{
    private Integer userId;
    private String emailId;
    private String password;
    private String firstName;
    private String lastName;

    @Override
    public String toString() {
        return "User [userId=" + userId + ", emailId=" + emailId
                + ", password=" + password + ", firstName=" + firstName
                + ", lastName=" + lastName + "]";
    }
    //setters and getters

    public Integer getUserId() {
        return userId;
    }

    public String getEmailId() {
        return emailId;
    }

    public String getPassword() {
        return password;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
}